import Consept from './Condition';
import {expect} from 'chai';

let concept;

describe('Concept class', () => {
  beforeEach(() => {
    concept = new Consept();
  });

  it('should evaluate simple rule', () => {
    let rule = {'==': [1, 1]};
    let data = {};
    expect(concept.evaluateRule(rule, data)).to.be.true;

    rule = {'==': [1, 2]};
    data = {};
    expect(concept.evaluateRule(rule, data)).to.be.false;
  });

  it('should evaluate rule with data', () => {
    let rule = {
      '==': [
        {
          'var': ['test_key']
        },
        'test_value'
      ]
    };
    let data = {test_key: 'test_value'};
    expect(concept.evaluateRule(rule, data)).to.be.true;

    rule = {'==': [{'var': ['test_key']}, 'test_value']};
    data = {test_key: 'wrong_test_value'};
    expect(concept.evaluateRule(rule, data)).to.be.false;
  });


  it('should evaluate rule with data.key', () => {
    let rule = {
      '==': [
        {'var': ['user.age']},
        [25],
      ]
    };
    let data = {
      user: {
        age: 25
      }
    };
    expect(concept.evaluateRule(rule, data)).to.be.true;

    data.user.age++;
    expect(concept.evaluateRule(rule, data)).to.be.false;

  });

  it('should evaluate date.key with default value', () => {
    let rule = {
      '==': [
        {'var': ['user.age', 20]},
        [25],
      ]
    };
    let data = undefined;
    expect(concept.evaluateRule(rule, data)).to.be.false;

    data = {};
    expect(concept.evaluateRule(rule, data)).to.be.false;

    data = {
      user: {}
    };
    expect(concept.evaluateRule(rule, data)).to.be.false;

    data = {
      user: {
        age: undefined
      }
    };
    expect(concept.evaluateRule(rule, data)).to.be.false;

    data = {
      user: {
        age: 21
      }
    };
    expect(concept.evaluateRule(rule, data)).to.be.false;

    data = {
      user: {
        age: 25
      }
    };
    expect(concept.evaluateRule(rule, data)).to.be.true;

    rule = {
      '==': [
        {'var': ['user.age', 25]},
        [25],
      ]
    };
    data = undefined;
    expect(concept.evaluateRule(rule, data)).to.be.true;
  });

  it('should evaluate nested rule with data.key', () => {
    let firstRule = {
      '>': [
        {'var': ['user.age', 20]},
        25
      ]
    };
    let secondRule = {
      '==': [
        {'var': ['user.name', '']},
        'Jon'
      ]
    };
    let rule = {
      'and': [
        firstRule,
        secondRule,
      ]
    };

    let data = {
      user: {
        age: 'wrong  age',
        name: 'wrong name'
      }
    };
    expect(concept.evaluateRule(rule, data)).to.be.false;

    data = {
      user: {
        age: 'wrong  age',
        name: 'Jon'
      }
    };
    expect(concept.evaluateRule(rule, data)).to.be.false;

    data = {
      user: {
        age: '30',
        name: 'Jon'
      }
    };
    expect(concept.evaluateRule(rule, data)).to.be.true;
  });

  it('should evaluate nested rules with N arguments', () => {
    let rule = {
      'and': [true, true, true, true, false]
    };
    expect(concept.evaluateRule(rule, {})).to.be.false;

    rule = {
      'and': [true, true, true, true, true]
    };
    expect(concept.evaluateRule(rule, {})).to.be.true;

    rule = {
      'or': [false, false, false, false, false]
    };
    expect(concept.evaluateRule(rule, {})).to.be.false;

    rule = {
      'or': [false, false, false, false, true]
    };
    expect(concept.evaluateRule(rule, {})).to.be.true;

  });


});