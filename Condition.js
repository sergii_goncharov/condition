import jsonLogic from 'json-logic-js';

class Condition {
  constructor() {
  }

  /*
  *
  * @param {Object} rule - object like: {'==': [variable, constant]}
  *   variable can be taken from context this way: {'var': ['user.name']},
  *   or with default value like this {'var': ['user.age', 16]}
  *   example: {'==': [{'var': ['user.name']}, 'Jon']}
  *   rule can be nested: {'and': [rule1, rule2, rule3]}
  * @param {Object} context - data to use to evaluate condition
  *
  * @return {boolean}
  *
  * */
  evaluateRule(rule, context) {
    return jsonLogic.apply(rule, context);
  }

}

if(typeof module !== 'undefined' && module.exports){
  module.exports = Condition;
} else {
  window.Condition = Condition;
}

